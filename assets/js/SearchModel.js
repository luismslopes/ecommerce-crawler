/**
 * @jsx React.DOM
 */
/*jshint quotmark:false */
/*jshint white:false */
/*jshint trailing:false */
/*jshint newcap:false */

define(['app/utils'], function (Utils) {
  'use strict';

  // Generic "model" object. You can use whatever
  // framework you want. For this application it
  // may not even be worth separating this logic
  // out, but we do this to demonstrate one way to
  // separate out parts of your application.
  var SearchModel = function (url, socket) {
    this.url = url;
    this.socket = socket;
    this.onChanges = [];
    this.sites = [];
  };

  SearchModel.prototype.set = function(data) {
    this.sites = data;
    this.inform();
  };

  SearchModel.prototype.subscribe = function (onChange) {
    this.onChanges.push(onChange);
  };

  SearchModel.prototype.inform = function () {
    this.onChanges.forEach(function (cb) { cb(); });
  };

  SearchModel.prototype.addSite = function (title) {
    var todo = {
      uid: Utils.uuid(), //not used anymore
      title: title,
      completed: false
    };

    this.socket.post(this.url, todo, function whenServerResponds(data) {
      this.sites = this.sites.concat(data);
      console.log('Message posted :: ', data);
    }.bind(this));

  };


  SearchModel.prototype.destroy = function (todo) {
    this.socket.delete(this.url + '/' + todo.id, function whenServerResponds(data) {
      console.log('Message destroy :: ', data);
    });
  };

  SearchModel.prototype.save = function (todoToSave, text) {
    this.socket.put(this.url + '/' + todoToSave.id, {title: text}, function whenServerResponds(data) {
      console.log('Message save :: ', data);
    });
  };



  return SearchModel;

});
