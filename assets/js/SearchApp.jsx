/**
 * @jsx React.DOM
 */
/*jshint quotmark:false */
/*jshint white:false */
/*jshint trailing:false */
/*jshint newcap:false */
/*global React, Router*/
define(['react',  'app/Site', 'app/utils'], function (React,  Site, Utils) {
  'use strict';

  var ENTER_KEY = 13;

  var SearchApp = React.createClass({
    getInitialState: function () {
      return {
        editing: null
      };
    },


    render: function () {
      var main;
      var sites = this.props.model.sites;



      var sitesList = sites.map(function (site) {
        return (
          <Site
            key={site.id}
            site={site}
          />
        );
      }, this);


      if (sites.length) {
        main = (
          <section id="main">
            <div id="sites-list" class="col-md-12">
              {sitesList}
            </div>
          </section>
        );
      }

      return (
        <div>
          <header id="header">
            <h1>Scavenger</h1>
          </header>
          {main}
        </div>
      );
    }
  });

  return SearchApp;

});
