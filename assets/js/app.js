requirejs.config({
    paths: {
        'react': '/bower_components/react/react-with-addons',
        'app': '/js'
    }
});

require(['react', 'app/SearchModel', 'app/SearchApp'],
    function (React, SearchModel, SearchApp) {

        // as soon as this file is loaded, connect automatically,
        var socket = io.connect();

        var model = new SearchModel('/search', socket);

        function render() {

            React.renderComponent(
                SearchApp({model: model}),
                document.getElementById('scavenger_results')
            );
        }


        console.log('Connecting to Sails.js...');

        // Subscribe to updates (a sails get or post will auto subscribe to updates)
        socket.get('/search', function (message) {

            console.log('Listening...' + JSON.stringify(message));

            // initialize the view with the data property
            model.set(message);
            model.subscribe(render);
            render();

        });

        socket.on('addSiteResult', function whenMessageRecevied(message) {
            console.log('New comet message received :: ', message);

            //ADICIONAR À LISTA do model

        });


    }); //require