/**
 * @jsx React.DOM
 */
/*jshint quotmark: false */
/*jshint white: false */
/*jshint trailing: false */
/*jshint newcap: false */
/*global React */
define(['react'], function (React) {
    'use strict';

    var ESCAPE_KEY = 27;
    var ENTER_KEY = 13;

    var Site = React.createClass({


        getInitialState: function () {
            return {domain: this.props.site.domain};
        },

        /**
         * This is a completely optional performance enhancement that you can implement
         * on any React component. If you were to delete this method the app would still
         * work correctly (and still be very performant!), we just use it as an example
         * of how little code it takes to get an order of magnitude performance improvement.
         */
        shouldComponentUpdate: function (nextProps, nextState) {
            return (
                nextProps.site !== this.props.site ||
                nextProps.editing !== this.props.editing ||
                nextState.editText !== this.state.editText
                );
        },

        render: function () {
            return (
                <div className={React.addons.classSet({
                    editing: this.props.editing,
                    row: true
                })}>
                    <div className="view col-md-2">
           {this.props.domain}
                    </div>
                    <div className="view col-md-2">
           {this.props.platform.name}
                    </div>
                </div>
                );
        }
    });

    return Site;

});
