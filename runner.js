/*
 ## EXAMPLES OF PARSERS


 #### Attribution with words
 {
 property: 'platform',
 words: [
 'magento', 'shopify', 'prestashop', 'woocommerce', 'bigcommerce', 'demandware', 'opencart', 'volusion',
 'oscommerce', 'hybris', 'wordpress'
 ]
 }
 returns {platform: shopify}


 #### Attribution with Regex
 {
 property: 'platform',
 regexes: [
 "ui.\w+", //palavras começadas com ui
 ]
 }
 returns {platform: woocommerce}


 #### Attribution with Function
 {
 property: 'name',
 functions: [
 '$("title").text();'
 ]
 }

 returns {name: title}


 #### Matching with Functions
 {
 property: "foo",
 value: "bar",
 functions: [
 function(){
 return false;
 },
 function(){
 return true;
 }
 ]
 },

 returns {foo: bar}

 #### Autonomous with Remote
 {
 url: "http://data.alexa.com/data?cli=10&dat=snbamz&url=www.google.com",
 callbacks: [
 function(){
 return {foo:"bar"}
 }
 ]
 },

 */



var alexaParser = {
    url: "http://data.alexa.com/data?cli=10&dat=snbamz&url=:domain",
    functions: [
        function () {

            var retobj = {};

            var xmlDoc = XMLParser.parseXml($.html());

            retobj["email"] = XMLParser.attrInNode(xmlDoc.get('//email'), "addr");

            retobj["owner"] = XMLParser.attrInNode(xmlDoc.get('//owner'), "name");


            var xmlnode = xmlDoc.get('//site');
            retobj["storename"] = XMLParser.attrInNode(xmlnode, "title");
            if (retobj["storename"] == "") retobj["storename"] = XMLParser.attrInNode(xmlnode, "text");


            retobj["domain"] = XMLParser.attrInNode(xmlDoc.get('//alexa'), "url");

            //visitas
            xmlnode = xmlDoc.get("//country");
            retobj["country"] = XMLParser.attrInNode(xmlnode, "name");
            retobj["rank_pt"] = XMLParser.attrInNode(xmlnode, "rank");
            retobj["rank_global"] = XMLParser.attrInNode(xmlDoc.get("//reach"), "rank");

            //contactos
            retobj["phone"] = XMLParser.attrInNode(xmlDoc.get("//phone"), "number");

            xmlnode = xmlDoc.get("//addr");
            if (xmlnode) retobj["addr"] = {
                street: XMLParser.attrInNode(xmlnode, "street"),
                city: XMLParser.attrInNode(xmlnode, "city"),
                state: XMLParser.attrInNode(xmlnode, "state"),
                zip: XMLParser.attrInNode(xmlnode, "zip")

            };

            //General Product Category in [Electronics, other]


            //E-commerce Platform in [Prestashop, other]

            return retobj;
        }
    ]
};


Crawler = require('./api/services/crawler/Crawler');
var fs = require('fs');

(function () {


    console.log("READING URLS FILE...");
    var content = fs.readFileSync("/home/luis/Desktop/alexa1M.txt", {encoding: "UTF8"});
    var urls = content.split("\n");
    console.log("FINISHING FILE");

    var parsers = [
        //alexaParser,
        {
            property: "platform",
            value: "desk",
            html: {
                words: [".desk", "desk.com" ]
            },
            headers: {
                words: [".desk", "desk.com" ]
            }
        },
        {
            property: "platform",
            value: "zendesk",
            html: {
                words: [".zendesk", "zendesk.com" ]
            }
        },
        {
            property: "platform",
            value: "magento",
            html: {
                regex: ["skin/frontend/(?:default|(enterprise))", "version:\\1?Enterprise:Community", "^(?:Mage|VarienForm)$"],
                words: ["magentocommerce", ".magento", "js/mage" ]
            }
        },
        {
            property: "platform",
            value: "shopify",
            html: {
                words: "shopify",
                regex: ["<link[^>]+href=\"cdn.shopify.com\".*>"]
            }
        },
        {
            property: "platform",
            value: "oscommerce",
            "html": {
                words: [".oscommerce"],
                regex: ["(?:<a[^>]*(?:\\?|&)osCsid|Powered by (?:<[^>]+>)?osCommerce</a>|<[^>]+class=\"[^>]*infoBoxHeading)"]
            },
            headers: {
                "only": "Set-Cookie",
                "regex": ["osCsid="]
            }
        },
        {
            property: "platform",
            value: "bigcommerce",
            html:
            {
                regex: ["<link href=[^>]+cdn\\d+\\.bigcommerce\\.com/v"],
                words: ["mybigcommerce", "bigcommerce"]
            }
        }

    ];


    //lista de urls
    //nivel de profundidade = 1
    //parse os iniciais - sim
    //nao procurar urls internos - sim

    var c = new Crawler(
        //urls,
        ["http://rally.org/", "http://hoteltonight.com/"],
        parsers,
        {
            showWhenFind: ["platform"], //must have all to stop
            maxLevel: 1,
            parseFirstLvl: true,
            crawlInternalUrls: false
        }
    );

    c.run();


})();
