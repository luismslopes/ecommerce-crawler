/**
 * FilterController
 *
 * @description :: Server-side logic for managing Filters
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    // a CREATE action  
    create: function(req, res, next) {

        var params = req.params.all();
        if(params["callbacks"] !== undefined){
            params["callbacks"] = params["callbacks"].split("|");
        }
        if(params["regexes"] !== undefined){
            params["regexes"] = params["regexes"].split("|");
        }

        Filter.create(params, function(err, Filter) {

            if (err) return next(err);

            res.status(201);

            res.json(Filter);

        });

    },

    // a FIND action
    find: function(req, res, next) {

        var id = req.param('id');

        var idShortCut = isShortcut(id);

        if (idShortCut === true) {
            return next();
        }

        if (id) {

            Filter.findOne(id, function(err, Filter) {

                if (Filter === undefined) return res.notFound();

                if (err) return next(err);

                res.json(Filter);

            });

        } else {

            var where = req.param('where');

            if (_.isString(where)) {
                where = JSON.parse(where);
            }

            var options = {
                limit: req.param('limit') || undefined,
                skip: req.param('skip') || undefined,
                sort: req.param('sort') || undefined,
                where: where || undefined
            };

            Filter.find(options, function(err, Filter) {

                if (Filter === undefined) return res.notFound();

                if (err) return next(err);

                res.json(Filter);

            });

        }

        function isShortcut(id) {
            if (id === 'find' || id === 'update' || id === 'create' || id === 'destroy') {
                return true;
            }
        }

    },

    // an UPDATE action
    update: function(req, res, next) {

        var criteria = {};

        var params = req.params.all();
        if(params["callbacks"] !== undefined){
            params["callbacks"] = params["callbacks"].split(",");
        }
        if(params["regexes"] !== undefined){
            params["regexes"] = params["regexes"].split(",");
        }

        criteria = _.merge({}, params, req.body);

        var id = req.param('id');

        if (!id) {
            return res.badRequest('No id provided.');
        }

        Filter.update(id, criteria, function(err, Filter) {

            if (Filter.length === 0) return res.notFound();

            if (err) return next(err);

            res.json(Filter);

        });

    },

    // a DESTROY action
    destroy: function(req, res, next) {

        var id = req.param('id');

        if (!id) {
            return res.badRequest('No id provided.');
        }

        Filter.findOne(id).done(function(err, result) {
            if (err) return res.serverError(err);

            if (!result) return res.notFound();

            Filter.destroy(id, function(err) {

                if (err) return next(err);

                return res.json(result);
            });

        });
    }
};

