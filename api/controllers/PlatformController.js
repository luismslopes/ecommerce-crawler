/**
 * PlatformController
 *
 * @description :: Server-side logic for managing platforms
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    // a CREATE action
    create: function(req, res, next) {

        var params = req.params.all();

        Platform.create(params, function(err, Platform) {

            if (err) return next(err);

            res.status(201);

            res.json(Platform);

        });

    },

    // a FIND action
    find: function(req, res, next) {

        var id = req.param('id');

        var idShortCut = isShortcut(id);

        if (idShortCut === true) {
            return next();
        }

        if (id) {

            Platform.findOne(id, function(err, Platform) {

                if (Platform === undefined) return res.notFound();

                if (err) return next(err);

                res.json(Platform);

            });

        } else {

            var where = req.param('where');

            if (_.isString(where)) {
                where = JSON.parse(where);
            }

            var options = {
                limit: req.param('limit') || undefined,
                skip: req.param('skip') || undefined,
                sort: req.param('sort') || undefined,
                where: where || undefined
            };

            Platform.find(options, function(err, Platform) {

                if (Platform === undefined) return res.notFound();

                if (err) return next(err);

                res.json(Platform);

            });

        }

        function isShortcut(id) {
            if (id === 'find' || id === 'update' || id === 'create' || id === 'destroy') {
                return true;
            }
        }

    },

    // an UPDATE action
    update: function(req, res, next) {

        var criteria = {};

        criteria = _.merge({}, req.params.all(), req.body);

        var id = req.param('id');

        if (!id) {
            return res.badRequest('No id provided.');
        }

        Platform.update(id, criteria, function(err, Platform) {

            if (Platform.length === 0) return res.notFound();

            if (err) return next(err);

            res.json(Platform);

        });

    },

    // a DESTROY action
    destroy: function(req, res, next) {

        var id = req.param('id');

        if (!id) {
            return res.badRequest('No id provided.');
        }

        Platform.findOne(id).done(function(err, result) {
            if (err) return res.serverError(err);

            if (!result) return res.notFound();

            Platform.destroy(id, function(err) {

                if (err) return next(err);

                return res.json(result);
            });

        });
    }
};

