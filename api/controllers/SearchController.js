/**
 * SearchController
 *
 * @description :: Server-side logic for managing Searchs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    // a CREATE action  
    create: function (req, res, next) {

        var params = req.params.all();
        if (params["url_list"] !== undefined) {
            params["url_list"] = params["url_list"].split("|");
        }
        if (params["searchfilters"] !== undefined) {
            params["searchfilters"] = params["searchfilters"].split("|");
        }

        Search.create(params, function (err, s) {

            if (err) return next(err);

            res.status(201);

            Search.find(s.id).populate("searchfilters").exec(function(err,resfil){
                var filtros = resfil[0].searchfilters;
                delete filtros["add"];
                delete filtros["remove"];
                console.log("filtros", filtros);

                var c = require('../services/CrawlerService')(filtros);
                c.run(s.url_list);

            });


            res.json(s);

        });

    },

    // a FIND action
    find: function (req, res, next) {

        var id = req.param('id');

        var idShortCut = isShortcut(id);

        if (idShortCut === true) {
            return next();
        }

        if (id) {

            Search.findOne(id, function (err, Search) {

                if (Search === undefined) return res.notFound();

                if (err) return next(err);

                res.json(Search);

            });

        } else {

            var where = req.param('where');

            if (_.isString(where)) {
                where = JSON.parse(where);
            }

            var options = {
                limit: req.param('limit') || undefined,
                skip: req.param('skip') || undefined,
                sort: req.param('sort') || undefined,
                where: where || undefined
            };

            Search.find(options, function (err, Search) {

                if (Search === undefined) return res.notFound();

                if (err) return next(err);

                res.json(Search);

            });

        }

        function isShortcut(id) {
            if (id === 'find' || id === 'update' || id === 'create' || id === 'destroy') {
                return true;
            }
        }

    },

    // an UPDATE action
    update: function (req, res, next) {

        var criteria = {};

        var params = req.params.all();
        if (params["url_list"] !== undefined) {
            params["url_list"] = params["url_list"].split(",");
        }

        criteria = _.merge({}, params, req.body);

        var id = req.param('id');

        if (!id) {
            return res.badRequest('No id provided.');
        }

        Search.update(id, criteria, function (err, Search) {

            if (Search.length === 0) return res.notFound();

            if (err) return next(err);

            res.json(Search);

        });

    },

    // a DESTROY action
    destroy: function (req, res, next) {

        var id = req.param('id');

        if (!id) {
            return res.badRequest('No id provided.');
        }

        Search.findOne(id).done(function (err, result) {
            if (err) return res.serverError(err);

            if (!result) return res.notFound();

            Search.destroy(id, function (err) {

                if (err) return next(err);

                return res.json(result);
            });

        });
    }

};

