/**
 * Filter.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    schema: true,

    attributes: {

        platforms:{
            collection: 'platform',
            via: 'parser'
        },


        property: { type: 'string' },

        value: { type: 'string' },

        url: { type: 'string' },

        regexes: { type: 'array' },

        functions: { type: 'array' }
    }




};

