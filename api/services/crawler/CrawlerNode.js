Url = require('url');
Request = require('request');


var events = require('events');
EventEmitter = new events.EventEmitter().setMaxListeners(20);

_ = require('underscore-node');


// Constructor
function CrawlerNode(crawler, url, level) {

    this.crawler = crawler;

    this.originUrl = ((typeof(level) == "undefined") ? null : Url.parse(url));
    this.internalUrlsQueue = ((typeof(url) == "undefined") ? {} : (_.isString(url) ? [url] : url));

    this.parentNode = null;

    this.childrenNodes = {};


    // always initialize all instance properties
    this.level = ((typeof(level) == "undefined") ? 0 : level);

}

//EVENTO QUANDO SUCESSO
EventEmitter.on('spider.finished.success', function (response, node) {

    if (node.level > 1 || (node.level == 1 && node.crawler.options.parseFirstLvl)) {
        node.crawler.parse(node, response);
    }

    if( node.crawler.options.crawlInternalUrls) {
        node.findUrls(response.body);
    }
    node.crawlNext();
});


//EVENTO QUANDO FALHA
EventEmitter.on('spider.finished.failed', function (response, node) {
    //handle FAILED result
    node.crawlNext()
});




// class methods
CrawlerNode.prototype.addChild = function (url) {

    var maxlevel = this.crawler.options.maxLevel;

    //verificar se é duplicado ou acima do nivel
    if (this.childrenNodes[url] || this.crawler.wasVisited(url) || (maxlevel != -1 && this.level >= maxlevel)) {
        return;
    }

    var node = new CrawlerNode(this.crawler, url, this.level + 1);
    node.setParent(this);
    this.childrenNodes[url] = node;
};

CrawlerNode.prototype.getNextChild = function () {

    var firstKey = _.keys(this.childrenNodes)[0];
    var retNode = this.childrenNodes[firstKey];

    delete this.childrenNodes[firstKey];
    return retNode;

};


CrawlerNode.prototype.setParent = function (node) {
    this.parentNode = node;
};
CrawlerNode.prototype.getParent = function () {
    return this.parentNode;
};


CrawlerNode.prototype.getNextUrl = function () {

    var url = null;

    if (this.internalUrlsQueue.length > 0) { //enquanto houver links deste site

        url = this.internalUrlsQueue.splice(0, 1)[0];
    }


    return url;

};


CrawlerNode.prototype.crawl = function (url) {


    if (this.crawler.wasVisited(url)) {
        return;
    }

    this.crawler.setVisited(url);

    console.log("-- ", url);

    var thisNode = this;

    //console.log("\n----crawling", url);

    /*var phantom = require('phantom');

    phantom.create(function (ph) {
        ph.createPage(function (page) {
            page.open(url, function (status) {
                EventEmitter.emit('spider.finished.success', response, thisNode);
                ph.exit();
            });
        });
    });*/

    Request(url, function (error, response) {
        if (!error) {


            // finished parsing page
            EventEmitter.emit('spider.finished.success', response, thisNode);


        } else {

             //console.log("Error while requesting", url);

            EventEmitter.emit('spider.finished.failed', response, thisNode);


        }
    });

};

CrawlerNode.prototype.crawlNext = function (jumpToParent) {

    jumpToParent = (typeof(jumpToParent) == "undefined" ? false : jumpToParent);


    if (!jumpToParent) {
        //se nao houver mais urls a pesquisar indica ao pai para continuar
        var url = this.getNextUrl();
        if (url) {
            this.crawl(url);
            return;
        }


        //se tiver filhos vai pesquisar nesses primeiro
        var nextNode = this.getNextChild();
        if (nextNode) {
            nextNode.crawlNext();
            return;
        }
    }

    //sem filhos e sem urls internos vai para o proximo no pai
    var parent = this.getParent();
    if (parent) {
        parent.crawlNext();
        return;

    }

};


CrawlerNode.prototype.findUrls = function (html) {


    //handle result
    // DESCARTO O CHEERIO PARA DESCOBRIR OS URLS PORQUE O REGEX DÁ BONS RESULTADOS E É MAIS RAPIDO
    /*var $ = Cheerio.load(html);
     $('a').each(function(i, element) {
     var link = element.attribs.href;
     addUrlToList(link);

     });
     */

    //search for absolute paths
    //USO REGEX PORQUE PODE HAVER URLS DENTRO DE JAVASCRIPT POR REDIRECCIONAMENTOS INDEFERIDOS
    var urlRegex = /(https?(:|%3A)?(\/|%2F)?(\/|%2F)?)?([\da-z\.-]+)\.([a-z\.]{2,10})([\/\w \.-?&]*)*\/?/g;
    var links = html.match(urlRegex);

    for (var a in links) {
        this.queueUrl(links[a]);
    }


    //search for relative paths  => TODOS INTERNOS
    var pathsRegex = /href="([^"]*?)#?([^"]*).*?/g;
    var links = html.match(pathsRegex);

    for (var i in links) {
        var link = links[i].replace('href="', "");

        this.queueUrl(link);
    }
};

CrawlerNode.prototype.queueUrl = function (url) {

    if (url == undefined) {
        return;
    }

    //tirar qualquer espaço em branco
    url = url.split(" ")[0];
    url = unescape(url.trim());

    //ignorar os href dummy
    if (url.indexOf("#") == 0)
        return;

    //verificar se tem alguma palavra proibida
    if (this.crawler.isUrlForbidden(url)) {
        return;
    }


    // compor link
    var validlink = false;
    if (url.indexOf("/") == 0) { //se comecar com barra tem de se lhe acrescentar o dominio inicial

        var host = this.originUrl.hostname;
        host = (host[host.length - 1] == "/" ? host.slice(0, -1) : host);

        url = host + url;
        validlink = true;
    }
    if (url.indexOf("www") == 0 || url.indexOf("http") == 0) {
        validlink = true;
    }
    if (validlink && url.indexOf("http://") == -1 && url.indexOf("https://") == -1) {
        url = "http://" + url;
    }

    if (!validlink) {
        return;
    }


    //Verificar se é interno ou externo
    var host = this.originUrl.hostname.replace("www.", "");


    //console.log("host", host)
    if (url.indexOf(host) != -1) { //url interno

        //verificar se é duplicado
        // nao pode existir tanto nos que estão para ver como nos que já foram vistos
        if (this.crawler.wasVisited(url) || this.internalUrlsQueue.indexOf(url) != -1) {
            return;
        }

        //colocar na fila para ver
        this.internalUrlsQueue.push(url);

    } else {     //url externo (filho - proximo nivel)

        if (this.level == this.crawler.options.maxLevel && this.crawler.options.quickParseLastLeaf) { //se for para investigar as ultimas folhas
            //coloco nulo para não lhe dar continuidade, mas tem de estar aqui par nao duplicar

            var parsedurl = Url.parse(url);
            parsedurl = parsedurl.href;

            if (this.crawler.wasVisited(parsedurl))
                return;


            var node = new CrawlerNode(this.crawler, parsedurl, this.level + 1);
            node.crawl(parsedurl);
        } else {


            this.addChild(url);
        }
    }

};

// export the class
module.exports = CrawlerNode;