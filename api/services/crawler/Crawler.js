//LIB
CrawlerNode = require("./CrawlerNode");
Scrapper = require("./Scrapper");
Url = require('url');


BLACK_EXTENSIONS = [".jpg", ".jpeg", ".gif", ".png", ".ico", ".css", ".js", ".swf", ".gov"];

BLACK_WORDS = ["google", "amazon", "facebook", "vimeo", "youtube", "apple", "twitter", "linkedin", "pinterest",
    "instagram", "apache.org", "doubleclick", "w3", "windowsphone", "slidesjs", "schema.org", "jquery", "opensource",
    "cookie", "github", "blog.", "highcharts", "images.", "bootstrap", "track.", "cloudfront", "ad.", "youtu.be",
    "adobe", "affiliate", "vk", "verisign", "php", "opera.com", "mail.", "typeform", "s1.", "s2.", "s3."];


// Constructor
function Crawler(urls, parsers, options) {

    if (_.isString(urls)) {
        urls = [urls];
    } else if (!_.isArray(urls)) {
        console.log("First Argument must be a url string or an array of them");
        return;
    }

    if (typeof(parsers) == "undefined") return;

    this.Scrapper = new Scrapper(parsers, this, this.onFinishParser);


    this.options = _.defaults(options,
        {
            showWhenFind: [],
            maxLevel: 0, //don't search internals
            parseFirstLvl: true,
            quickParseLastLeaf: false,
            crawlInternalUrls: true
        });

    this.visited = {};


    this.initialNode = new CrawlerNode(this);

    console.log("BUILDING FIRST NODES");
    for (var i in urls) {
        var url = urls[i];

        if (url.indexOf("www") == -1) url = "www." + url;
        url = Url.parse(url);
        url.protocol = "http:";
        url.slashes = true;
        url.hostname = urls[i];
        url.pathname = "/";


        this.initialNode.addChild(Url.format(url));
    }
    console.log("START TO CRAWL");

}


Crawler.prototype.wasVisited = function (url) {

    return (this.visited[url] != undefined);
};


Crawler.prototype.setVisited = function (url) {

    this.visited[url] = url;
};


Crawler.prototype.run = function () {

    this.initialNode.crawlNext();
};


Crawler.prototype.parse = function (node, response) {
    this.Scrapper.queueNode(node, response);
};


Crawler.prototype.onFinishParser = function (result, crawler) {

    if (!_.isEmpty(result)) {
        var stopLen = crawler.options.showWhenFind.length;
        if (stopLen == 0 ||
            _.intersection(_.keys(result), crawler.options.showWhenFind).length == stopLen) {

            //node.crawlNext(true);
            console.log(result);
        }
    }
};


Crawler.prototype.isUrlForbidden = function (url) {
    var forbiddenArray = BLACK_EXTENSIONS.concat(BLACK_WORDS);

    for (var i in forbiddenArray) {
        if (url.indexOf(forbiddenArray[i]) != -1) {
            return true;
        }
    }
};


// export the class
module.exports = Crawler;