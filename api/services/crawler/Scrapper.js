/**
 * Scrapper.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

Cheerio = require('cheerio');
Request = require('request');


XMLParser = require('libxmljs');
XMLParser.attrInNode = function (xmlnode, property) {
    if (xmlnode) return xmlnode.attr(property).value();
    return "";
};


_ = require('underscore-node');


// Constructor
function Scrapper(parsers, crawler, finishCallback) {

    this.parsers = parsers;


    this.crawler = crawler;

    this.onFinish = ((typeof(finishCallback) == "undefined") ? function (retObj) {
        return console.log(retObj);
    } : finishCallback);

    this.parserQueue = {};
}

Scrapper.prototype.queueNode = function (node, response) {

    var referenceUrl = node.originUrl.hostname;

    if (response.body == undefined)
        return;


    if (!this.parserQueue[referenceUrl]) {
        this.parserQueue[referenceUrl] = {queue: this.parsers.slice(0), response: response, retObj: {}}
    }

    this.parserNext(referenceUrl);
};

Scrapper.prototype.parserNext = function (referenceUrl, response) {

    var nodeToParse = this.parserQueue[referenceUrl];
    var nextParser = _.first(nodeToParse.queue);

    if (_.isEmpty(nextParser)) {
        this.onFinish(nodeToParse.retObj, this.crawler);
    } else {


        nodeToParse.queue = nodeToParse.queue.slice(1);

        response = ((typeof(response) == "undefined") ? nodeToParse.response : response);

        this.executeParser(referenceUrl, nextParser, response);


    }
};


function parseRegexAndWords(subparse, string) {
    var res_val = null, exp ="";

    //console.log(subparse);

    if (subparse.words) {

        if (typeof(subparse.words) == "string") {
            subparse.words = [subparse.words];
        }

        exp = subparse.words.join("|");
        exp.replace(".", "\\.")
    }


    if (subparse.regex) {
        if (typeof(subparse.regex) == "string") {
            subparse.regex = [subparse.regex];
        }

        if (res_val == null && subparse.regex) {

            /*for (var i in subparse.regex) {
                var exp = subparse.regex[i];

                var patt = new RegExp(exp);
                console.log("Execurar regex", patt, patt.exec(string));
                res_val = patt.exec(string);

            } */

            if(exp != ""){
                exp = exp + "|";
            }
            exp = exp + subparse.regex.join("|")

        }
    }

    if(exp != "") {
        var patt = new RegExp(exp, "ig");
       // console.log("Execurar regex", patt, patt.exec(string));
        res_val = patt.exec(string);
    }
    return res_val;
}

Scrapper.prototype.parseHtml = function (parser, response) {

    if (!parser.html) return {};

    var res = parseRegexAndWords(parser.html, response.body);


    var retObj = {};

    if (res != null) {
        retObj[parser.property] = res;
        if (parser.value) {
            retObj[parser.property] = parser.value;
        }
    }


    return retObj;

};


Scrapper.prototype.parseHeaders = function (parser, response) {

    if (!parser.headers) return {};



    var res = "";

    if (parser.headers.only != null) {
        res = parseRegexAndWords(parser.headers, response.headers[parser.headers.only]);
    } else { // ver em todos
        //console.log("ver em cada um ", response.headers.length);
        /*for (var hi in response.headers) {
            res = parseRegexAndWords(parser.headers, response.headers[hi]);
            if(res != null){
                break;
            }
        }        */
        var headersString = _.flatten(_.pairs(response.headers)).join(";;");
        res = parseRegexAndWords(parser.headers, headersString); //uma unica string com todos os headers
    }

    var ret = {};
    if (res != null) {
        ret[parser.property] = res;
        if (parser.value) {
            ret[parser.property] = parser.value;
        }
        return ret;
    }

//    console.log("RETURN", ret);
    return ret;

};




Scrapper.prototype.parserWithFunctions = function (parser, response) {

    var ret_obj = {};


    //VARIAVEIS PARA SEREM USADAS EM FUNCOES
    var $ = Cheerio.load(response.body);
    var headers = response.headers;

    //  console.log("Functions", parser.functions);
    for (var i in parser.functions) {
        var func = parser.functions[i];

        try {
            if ($) {
                var res = null;
                if (typeof(func) == "string") {

                    res = eval(func);
                } else {
                    func = "(" + func.toString() + ")";
                    res = eval(func)();
                }


            }
        } catch (e) {
            //console.log("Error while executing parser function", parser);
        }
    }

    if (res && parser.property) {
        ret_obj[parser.property] = res;
        return ret_obj;
    } else {
        return res;
    }

    return null;
};


Scrapper.prototype.executeParser = function (referenceUrl, parser, response, ignoreRemote) {

    ignoreRemote = ((typeof(ignoreRemote) == "undefined") ? false : ignoreRemote);

    referenceUrl = referenceUrl.replace("/", "");


    var ret_obj = {domain: referenceUrl};


    //executar os filtros
    var parserkeys = _.keys(parser);
    for (var pi in parserkeys) {

        key = parserkeys[pi];
        switch (key) {
            case "html":
                _.extend(ret_obj, this.parseHtml(parser, response));
            case "headers":
                _.extend(ret_obj, this.parseHeaders(parser, response));
            case "functions":
                try {
                    if (parser.url && !ignoreRemote) { //é um remote por isso vou diferir até o remote estar completo

                        this.parserWithRemote(referenceUrl, parser);
                        return;
                    } else {
                        _.extend(ret_obj, this.parserWithFunctions(parser, response));
                    }
                } catch (e) {
                    console.log("Error while running function parser")
                }
        }

    }


    _.extend(this.parserQueue[referenceUrl].retObj, ret_obj);


     this.parserNext(referenceUrl);

};


Scrapper.prototype.parserWithRemote = function (referenceUrl, parser) {
    //http://www.similarweb.com/website/

    if (typeof(parser) == "undefined" || parser.url == undefined) {
        return;
    }

    var url = parser.url;

    //replace de variáveis
    url = url.replace(":domain", referenceUrl);

    var Scrapper = this
    Request(url, function (error, response, body) {

        Scrapper.executeParser(referenceUrl, parser, response, true);

    });


};

Scrapper.prototype.parsePlatform = function (referenceUrl, parser) {

    return {}
};


// export the class
module.exports = Scrapper;


